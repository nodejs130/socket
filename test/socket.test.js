const { createServer } = require('http');
const { Server } = require('socket.io');
const Client = require('socket.io-client');

describe('Testing Socket.io', () => {
  let io, serverSocket, clientSocket;

  // Antes de empezar los test creamos el servidor
  beforeAll((done) => {
    const httpServer = createServer();
    io = new Server(httpServer);

    httpServer.listen(() => {
      const port = httpServer.address().port;

      clientSocket = new Client(`http://localhost:${port}`);

      io.on('connection', (socket) => {
        serverSocket = socket;
      });

      clientSocket.on('connect', done);
    });
  });

  afterAll(() => {
    clientSocket.close();
    io.close();
  });

  test('Test event', (done) => {
    clientSocket.on('greeting', (data) => {
      try {
        expect(data).toBe('Hola');
        done();
      } catch (error) {
        done(error);
      }
    });

    serverSocket.emit('greeting', 'Hola');
  });

  test('Testing callbacks (acknoledgements)', (done) => {
    serverSocket.on('bark', (callback) => {
      callback('Woof!');
    });

    clientSocket.emit('bark', (arg) => {
      try {
        expect(arg).toBe('Woof!');
        done();
      } catch (error) {
        done(error);
      }
    });
  });
});
