const express = require('express');
const path = require('path');
const { createServer } = require('http');
const { Server } = require('socket.io');

const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer);

app.use(express.static(path.join(__dirname, 'views')));

const socketsOnline = [];

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/views/index.html');
});

io.on('connection', (socket) => {
  // console.log('Clientes conectados', io.engine.clientsCount);
  // console.log('Id del socket conectado', socket.id);
  // socket.on('disconnect', () => {
  //   console.log(`El socket ${socket.id} se ha desconectado`);
  // });
  // socket.conn.once('upgrade', () => {
  //   console.log(
  //     `Hemos pasado de HTTP Long-Polling a ${socket.conn.transport.name}`
  //   );
  // });

  socketsOnline.push(socket.id);

  socket.connectedRoom = '';

  // Emision basica
  socket.emit('welcome', `Estas conectado: ${socket.id}`);

  socket.on('server', (data) => {
    console.log(data);
  });

  // Emision a todos
  io.emit('everyone', socket.id + ' se ha conectado');

  // Emision a uno solo
  socket.on('last', (data) => {
    const lastScokect = socketsOnline[socketsOnline.length - 1];

    io.to(lastScokect).emit('salute', data);
  });

  // on, once, off
  socket.emit('on', '');
  socket.emit('on', '');

  socket.emit('once', '');
  socket.emit('once', '');

  socket.emit('off', '');
  setTimeout(() => {
    console.log('Emite off nuevamente');
    socket.emit('off', '');
  }, 3000);

  socket.on('circle_position', (data) => {
    socket.broadcast.emit('move_circle', data);
  });

  socket.on('connect_to_room', (data) => {
    socket.leave(socket.connectedRoom);

    switch (data) {
      case 'room1':
        socket.join('room1');
        socket.connectedRoom = 'room1';
        break;

      case 'room2':
        socket.join('room2');
        socket.connectedRoom = 'room2';
        break;

      case 'room3':
        socket.join('room3');
        socket.connectedRoom = 'room3';
        break;
    }
  });

  socket.on('message', (data) => {
    const room = socket.connectedRoom;

    io.to(room).emit('send_message', {
      message: data,
      room,
    });
  });

  socket.on('disconnect', () => {
    const eliminar = socketsOnline.findIndex((id) => id == socket.id);
    socketsOnline.splice(eliminar, 1);
    io.emit('desconectado', `${socket.id} se ha desconectado`);
  });
});

const teachers = io.of('teachers');
const students = io.of('students');

teachers.on('connection', (socket) => {
  console.log(`${socket.id} se ha conectado a la sala de profes`);

  socket.on('send-message', (data) => {
    teachers.emit('message', data);
  });
});

students.on('connection', (socket) => {
  console.log(`${socket.id} se ha conectado a la sala de estudiantes`);

  socket.on('send-message', (data) => {
    students.emit('message', data);
  });
});

httpServer.listen(3000, () => {
  console.log(`Servidor corriendo en http://localhost:3000`);
});
