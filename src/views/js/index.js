const socket = io();
const user = prompt('Escribe tu usuario');

// function checkSocketStatus() {
//   console.log('Estado del socket: ', socket.connected);
// }

// socket.on('connect', () => {
//   console.log('El socket se ha conectado ' + socket.id);
//   checkSocketStatus();
// });

// socket.on('connect_error', () => {
//   console.log('No puede conectarme');
// });

// socket.on('disconnect', () => {
//   console.log('El socket se ha desconectado ' + socket.id);
//   checkSocketStatus();
// });

// socket.io.on('reconnect_attempt', () => {
//   console.log('Estoy intentando reconectarme');
// });

// socket.io.on('reconnect', () => {
//   console.log('Me he vuelto a conectar');
// });

const listener = () => {
  console.log('Se apaga el evento');
};

socket.on('welcome', (data) => {
  // text.textContent = data;
  console.log(data);
});

socket.on('everyone', (data) => {
  console.log(data);
});

socket.on('salute', (data) => {
  console.log(data);
});

socket.on('desconectado', (data) => {
  console.log(data);
});

// on, once, off
socket.on('on', () => {
  console.log('Se emite varias veces.');
});

socket.once('once', () => {
  console.log('Se emite una sola vez');
});

socket.on('off', listener);

// const emitToServer = document.querySelector('#emit-to-server');
// emitToServer.addEventListener('click', () => {
//   socket.emit('server', 'Hola, server');
// });

// const emitToLast = document.querySelector('#emit-to-last');
// emitToLast.addEventListener('click', () => {
//   socket.emit('last', 'Hola');
// });

setTimeout(() => {
  socket.off('off', listener);
}, 2000);

const drawCircle = (data) => {
  circle.style.top = data.top;
  circle.style.left = data.left;
};

const drag = (e) => {
  const data = {
    top: e.clientY + 'px',
    left: e.clientX + 'px',
  };

  drawCircle(data);
  socket.emit('circle_position', data);

  // circle.style.top = clientY + 'px';
  // circle.style.left = clientX + 'px';
};

const circle = document.querySelector('#circle');
document.addEventListener('mousedown', (e) => {
  document.addEventListener('mousemove', drag);
});
document.addEventListener('mouseup', (e) => {
  document.removeEventListener('mousemove', drag);
});

socket.on('move_circle', (data) => {
  drawCircle(data);
});

const connectRoom1 = document.querySelector('#connectRoom1');
const connectRoom2 = document.querySelector('#connectRoom2');
const connectRoom3 = document.querySelector('#connectRoom3');

// Eventos para que al hacer click se conecte a la sala
// connectRoom1.addEventListener('click', () => {
//   socket.emit('connect_to_room', 'room1');
// });
// connectRoom2.addEventListener('click', () => {
//   socket.emit('connect_to_room', 'room2');
// });
// connectRoom3.addEventListener('click', () => {
//   socket.emit('connect_to_room', 'room3');
// });

// enviar mensaje
// const sendMessage = document.querySelector('#sendMessage');
// sendMessage.addEventListener('click', () => {
//   const message = prompt('Escribe tu mensaje');
//   socket.emit('message', message);
// });

// Recibir el evento del mensaje
socket.on('send_message', (data) => {
  const { room, message } = data;

  const li = document.createElement('li');
  li.textContent = message;

  document.querySelector(`#${room}`).append(li);
});

const profes = ['lisa', 'maggie', 'bart'];

let socketNamespace, group;

const chat = document.querySelector('#chat');
const namespace = document.querySelector('#namespace');

if (profes.includes(user)) {
  socketNamespace = io('/teachers');
  group = 'teachers';
} else {
  socketNamespace = io('/students');
  group = 'students';
}

socketNamespace.on('connect', () => {
  namespace.textContent = group;
});

// Logica de envio de mensajes
const sendMessage = document.querySelector('#sendMessage');
sendMessage.addEventListener('click', () => {
  const message = prompt('Escribe tu mensaje');

  socketNamespace.emit('send-message', {
    message,
    user,
  });
});

socketNamespace.on('message', (data) => {
  const { user, message } = data;

  const li = document.createElement('li');
  li.textContent = `${user}: ${message}`;

  chat.append(li);
});
